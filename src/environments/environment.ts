// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  // Initialize Firebase
  firebase: {
    apiKey: "AIzaSyBDy0siOsFGzjO0bTr30NJva2bUlSdd2q4",
    authDomain: "masterbikes-e2d6e.firebaseapp.com",
    databaseURL: "https://masterbikes-e2d6e.firebaseio.com",
    projectId: "masterbikes-e2d6e",
    storageBucket: "",
    messagingSenderId: "1036322001365"
  }
};

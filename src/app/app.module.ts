import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';

// Firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

// Angular Material
import {
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatDialogModule
  } from "@angular/material";

// Archivos de rutas
import { APP_ROUTING } from './app.routes';

// Archivo de ambiente
import { environment } from '../environments/environment'

// Servicios
import { ParameterService } from "./modules/shared/services/parameter.service";
import { ShowHideService } from "./modules/shared/services/show-hide.service";
import { SharedService } from "./modules/shared/services/shared.service";

// Componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './modules/shared/components/navbar/navbar.component';
import { UserComponent } from './modules/user/user.component';
import { CatalogueComponent } from './modules/user/components/catalogue/catalogue.component';
import { LoginComponent } from './modules/login/login.component';
import { RepairRequestComponent } from './modules/user/components/repair-request/repair-request.component';
import { RentRequestComponent } from './modules/user/components/rent-request/rent-request.component';
import { RequestListComponent } from './modules/user/components/request-list/request-list.component';
import { ExpertComponent } from './modules/expert/expert.component';
import { RequestResponseComponent } from './modules/expert/components/request-response/request-response.component';
import { RequestDetailComponent } from './modules/expert/components/request-detail/request-detail.component';
import { StockComponent } from './modules/expert/components/stock/stock.component';
import { RegistryComponent } from './modules/registry/registry.component';
import { RepairDetailComponent } from './modules/user/components/repair-detail/repair-detail.component';
import { ExpertNavbarComponent } from './modules/shared/components/expert-navbar/expert-navbar.component';
import { RequestDetailUserComponent } from './modules/user/components/request-detail-user/request-detail-user.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UserComponent,
    CatalogueComponent,
    LoginComponent,
    RepairRequestComponent,
    RentRequestComponent,
    RequestListComponent,
    ExpertComponent,
    RequestResponseComponent,
    RequestDetailComponent,
    StockComponent,
    RegistryComponent,
    RepairDetailComponent,
    ExpertNavbarComponent,
    RequestDetailUserComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CommonModule,
    RouterModule,
    APP_ROUTING,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule
  ],
  providers: [
    ParameterService,
    ShowHideService,
    SharedService
  ],
  bootstrap: [
    AppComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }

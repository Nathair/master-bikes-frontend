import { Component, OnInit } from '@angular/core';
import { ShowHideService } from '../../services/show-hide.service'
import { ParameterService } from '../../services/parameter.service'

@Component({
  selector: 'app-expert-navbar',
  templateUrl: './expert-navbar.component.html',
  styleUrls: ['./expert-navbar.component.css']
})
export class ExpertNavbarComponent implements OnInit {

  constructor( private _showhide: ShowHideService, private _parameter: ParameterService ) { }

  ngOnInit() {
  }

  logout() {
    this._showhide.toggleNavbarVisible();
    this._parameter.logout();
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ExpertNavbarComponent } from './expert-navbar.component';
import { ShowHideService } from '../../services/show-hide.service';
import { ParameterService } from '../../services/parameter.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('ExpertNavbarComponent', () => {
  let component: ExpertNavbarComponent;
  let fixture: ComponentFixture<ExpertNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ ExpertNavbarComponent ],
      providers: [ ShowHideService, ParameterService, HttpClient, HttpHandler ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpertNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

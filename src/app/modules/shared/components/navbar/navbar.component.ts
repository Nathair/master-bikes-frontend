import { Component, OnInit } from '@angular/core';
import { ShowHideService } from '../../services/show-hide.service'
import { ParameterService } from '../../services/parameter.service'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor( private _showhide: ShowHideService, private _parameter: ParameterService ) { }

  ngOnInit() {
  }

  logout() {
    this._showhide.toggleNavbarVisible();
    this._parameter.logout();
  }

}

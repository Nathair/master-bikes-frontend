import { Injectable } from '@angular/core';

@Injectable()
export class ShowHideService {

  navbarVisible: boolean = false;

  constructor() { }

  // Servicio truco para algunas visualizaciones, ignorar por favor
  toggleNavbarVisible() {
    this.navbarVisible = !this.navbarVisible;
  }

}

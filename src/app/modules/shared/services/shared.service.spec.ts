import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { SharedService } from './shared.service';
import { ParameterService } from './parameter.service';
import { BehaviorSubject } from 'rxjs';

describe('SharedService', () => {

  const FirestoreStub = {
    collection: (name: string) => ({
      doc: (_id: string) => ({
        valueChanges: () => new BehaviorSubject({ foo: 'bar' }),
        set: (_d: any) => new Promise((resolve, _reject) => resolve()),
      }),
    }),
  };

  beforeEach(() => TestBed.configureTestingModule({
    providers: [ SharedService,
                  ParameterService,
                  HttpClient,
                  HttpHandler,
                  AngularFireAuthModule,
                  AngularFireStorageModule,
                  AngularFirestoreModule,
                  AngularFireModule,
                  { provide: AngularFirestore,
                    useValue: FirestoreStub }
                ]
  }));

  it('should be true', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeTruthy();
  });

  it('should be false', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeFalsy();
  });

  it('should be Defined', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeDefined();
  });

  it('should return user', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    const usuario = "usuario"
    expect(service.getUsers(usuario)).toBeTruthy();
  });

  it('should be created', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeTruthy();
  });

  it('should be created', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeTruthy();
  });

  it('should be created', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeTruthy();
  });

  it('should be created', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeTruthy();
  });

  it('should be created', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeTruthy();
  });

  it('should be created', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeTruthy();
  });

  it('should be created', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeTruthy();
  });

  it('should be created', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeTruthy();
  });

  it('should be created', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeTruthy();
  });

  it('should be created', () => {
    const service: SharedService = TestBed
    .get(SharedService);
    expect(service).toBeTruthy();
  });
});

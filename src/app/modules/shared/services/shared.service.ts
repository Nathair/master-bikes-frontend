import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ParameterService } from './parameter.service';
import { Observable } from 'rxjs/Rx';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable()
export class SharedService {

  // Variables
  resourceStock: string = "37815/Servicio_Bodega/WS_Stock?wsdl";
  resourceConvenio: string = "37815/ServicioMunicipalidad/webresources/ServiceRest/convenio/";
  resourceArriendo: string = "37816/ServicioVerificacion/webresources/VerificacionRest/Arriendo";
  resourceTipoReparacion: string = "37817/ServicioVerificacion/webresources/VerificacionRest/tipo/";
  resourceShimano: string = "37818/ServicioShimano/webresources/servicios/findAllProd";
  resourcePoduct: string = "products";
  resourceRequest: string = "requests";
  resourceUser: string = "users";

  constructor( public db: AngularFirestore, private http: HttpClient, private _parameter: ParameterService ) { }

  // Firebase Services

  getProducts() {
    // getting data from the db yo
    return this.db.collection( this.resourcePoduct ).valueChanges();
  }

  getRequests() {
    return this.db.collection( this.resourceRequest ).valueChanges();
  }

  getUsers( value: any ) {
    // return this.db.collection( this.resourceUser ).valueChanges();
    return this.db.collection( this.resourceUser, ref => ref.where("username", "==", value)).valueChanges();
  }

  // Spring Services

  getStockService(): Observable<any> {
    let url = this._parameter.getEndPoint() + this.resourceStock;
    return this.http.get( url )
  }

  getConvenioService(): Observable<any> {
    let url = this._parameter.getEndPoint() + this.resourceConvenio;
    return this.http.get( url )
  }

  getTipoReparacionService( value: any ): Observable<any> {
    let url = this._parameter.getEndPoint() + this.resourceTipoReparacion + value;
    return this.http.get( url )
  }
  getArriendoService(): Observable<any> {
    let url = this._parameter.getEndPoint() + this.resourceArriendo;
    return this.http.get( url )
  }

  getShimanoService(): Observable<any> {
    let url = this._parameter.getEndPoint() + this.resourceShimano;
    return this.http.get( url )
  }

}

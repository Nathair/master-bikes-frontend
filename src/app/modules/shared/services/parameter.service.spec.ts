import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { ParameterService } from './parameter.service';

describe('ParameterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ParameterService = TestBed
    .configureTestingModule({
      providers: [ ParameterService, HttpClient, HttpHandler ]
    })
    .get(ParameterService);
    expect(service).toBeTruthy();
  });
});

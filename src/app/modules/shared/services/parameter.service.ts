import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class ParameterService {

  // Ruta del json que contiene la url del servidor de los servicios
  parameterFile: string = "assets/config/parameter.json";

  constructor( private http: HttpClient ) { }

  logout() {
    this.setFlag("0");
    this.setRol("");
  }

  // getters & setters de session, ocupan menos espacio que la cache,
  // y no se pierden los datos al recargar la página
  setEndPoint( value: string ) {
    sessionStorage.setItem("endpoint", value);
  }

  getEndPoint() {
    return sessionStorage.getItem("endpoint");
  }

  setFlag( value: string ) {
    sessionStorage.setItem( "flag", value );
  }

  getFlag() {
    return sessionStorage.getItem( "flag" );
  }

  setRol( value: string ) {
    sessionStorage.setItem( "rol", value );
  }

  getRol() {
    return sessionStorage.getItem( "rol" );
  }

  // Metodo de obtencion de datos desde .json
  getParameters(): Observable<any> {
    return this.http.get( this.parameterFile )
  }

  // Se guardan datos en sessionStorage
  getValues() {
      this.getParameters()
          .subscribe( data => {
              this.setEndPoint(data.endpoint);
            },
            error => console.log( error )
          );
  }

}

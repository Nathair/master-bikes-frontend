import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-request-response',
  templateUrl: './request-response.component.html',
  styleUrls: ['./request-response.component.css']
})
export class RequestResponseComponent implements OnInit {

  constructor( private router: Router ) { }

  ngOnInit() {
  }

  redirect() {
    this.router.navigateByUrl('expert/request-detail');
  }

}

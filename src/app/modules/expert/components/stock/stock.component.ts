import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../shared/services/shared.service'

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {

  idProducto: any;
  respuesta: string;
  visible: boolean = false;
  listaStock: any[] = [];

  constructor( private _shared: SharedService ) { }

  ngOnInit() {
  }

  getCatalogo() {
      this._shared.getShimanoService()
          .subscribe( data => {
              this.listaStock = data.stock;
            },
            error => console.log( error )
          );
  }

  onClick() {
    this.visible = !this.visible;
  }

  otraConsulta() {
    this.idProducto = "";
    this.visible = !this.visible;
  }

  checkStock() {
    if (this.idProducto == "1") {
      this.respuesta = "Stock: 200";
    } else if (this.idProducto == "2") {
      this.respuesta = "Stock: 800";
    } else {
      this.respuesta = "Stock: 500";
    }
  }

}

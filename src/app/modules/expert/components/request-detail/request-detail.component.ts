import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-request-detail',
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.css']
})
export class RequestDetailComponent implements OnInit {

  visible: boolean = true;
  visibleDiv: boolean = true;
  problema: any = "piola"
  detalle: any = "la piola de mi bicicleta se ha cortado"

  constructor() { }

  ngOnInit() {
  }

  click() {
    this.visibleDiv = false;
  }

}

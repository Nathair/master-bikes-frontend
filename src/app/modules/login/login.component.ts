import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { SharedService } from "../shared/services/shared.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form;
  usuario: any;

  constructor( private fb: FormBuilder,
               private _shared: SharedService,
               private router: Router) {
                 this.form = fb.group({
                   username: ['', Validators.required],
                   password: ['', Validators.required]
                 })
               }

  ngOnInit() {
  }

  onLogin( fc: FormControl ) {
    this._shared.getUsers( fc.value.username ).subscribe( data => {
      this.usuario = data;
      // Se verifica que los datos coincidan con los de la bd
      if (fc.value.username == this.usuario[0].username && fc.value.password == this.usuario[0].password) {
        // Se verifica el rol del usuario
        if (this.usuario[0].rol == "experto") {
          this.router.navigateByUrl('/expert/request-response');
        } else if (this.usuario[0].rol == "usuario") {
          this.router.navigateByUrl('/user/catalogue');
        }
      } else {
        console.log("fuck off");
        return null;
      }
    })
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registry',
  templateUrl: './registry.component.html',
  styleUrls: ['./registry.component.css']
})
export class RegistryComponent implements OnInit {

  form;

  constructor( private fb: FormBuilder, private router: Router ) {
    this.form = fb.group({
      nombreCompleto: ['', Validators.required],
      correo: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ngOnInit() {
  }

  checkRegistry() {
    if (this.form.valid) {
      this.router.navigateByUrl('/login');
    }
  }

}

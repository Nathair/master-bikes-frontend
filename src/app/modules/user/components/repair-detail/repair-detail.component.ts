import { Component, OnInit, ViewEncapsulation, Inject, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material";
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-repair-detail',
  templateUrl: './repair-detail.component.html',
  styleUrls: ['./repair-detail.component.css']
})
export class RepairDetailComponent implements OnInit {

  visible: boolean = true;
  nombreProducto: any;
  precioProducto: any;
  stock: any;

  constructor( public db: AngularFirestore,
                public dialogRef: MatDialogRef<RepairDetailComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.nombreProducto = this.data.nombre;
    this.precioProducto = this.data.precio;
    this.stock = this.data.stock;
  }

  close() {
    this.dialogRef.close();
  }

}

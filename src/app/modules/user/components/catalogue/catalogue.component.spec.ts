import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialog,
  MAT_DIALOG_SCROLL_STRATEGY
  } from "@angular/material";
import { Overlay } from '@angular/cdk/overlay';
import { CatalogueComponent } from './catalogue.component';
import { SharedService } from '../../../shared/services/shared.service';
import { ParameterService } from '../../../shared/services/parameter.service';
import { ShowHideService } from '../../../shared/services/show-hide.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('CatalogueComponent', () => {
  let component: CatalogueComponent;
  let fixture: ComponentFixture<CatalogueComponent>;

  const FirestoreStub = {
    collection: (name: string) => ({
      doc: (_id: string) => ({
        valueChanges: () => new BehaviorSubject({ foo: 'bar' }),
        set: (_d: any) => new Promise((resolve, _reject) => resolve()),
      }),
    }),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogueComponent ],
      providers: [
        MatDialog,
        Overlay,
        { provide: MAT_DIALOG_SCROLL_STRATEGY },
        SharedService,
        AngularFireAuthModule,
        AngularFireStorageModule,
        AngularFirestoreModule,
        AngularFireModule,
        { provide: AngularFirestore,
          useValue: FirestoreStub },
          HttpClient,
          HttpHandler,
          ShowHideService,
          ParameterService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should open a dialog', () => {
    let object = {
      nombre: "rueda",
      precio: 5000,
      stock: 30
    }
    expect(component.obtenerProductos()).toHaveBeenCalled();
  });
});

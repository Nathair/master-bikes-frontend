import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material";
import { RepairDetailComponent } from '../repair-detail/repair-detail.component';
import { SharedService } from '../../../shared/services/shared.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.css']
})
export class CatalogueComponent implements OnInit {

  listaCatalogo: any[] = [];
  products: any;

  producto: {} = {
    nombre: "",
    precio: "",
    stock: ""
  }

  constructor( public dialog: MatDialog, private _shared: SharedService ) { }

  ngOnInit() {
    this.obtenerProductos();
  }

  // Esto abre el dialogo, y le pasa la data
  openDialog(value: {}): void {
    let dialogRef = this.dialog.open(RepairDetailComponent, {
      data: value
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  obtenerProductos() {
    // Consumiendo el 'servicio' de productos
    this._shared.getProducts().subscribe( data => {
      this.products = data;
    })
  }

  // Llamamos al servicio catalogo y almacenamos sus datos en el listado de catalogo
  // FIX: se cambia de Http a HttpClient por update de Angular 4 => 5
  // HttpCLient mapea los datos de manera automatica, se disminuye cantidad de codigo
  // y se optimiza el rendimiento
  getCatalogo() {
      this._shared.getShimanoService()
          .subscribe( data => {
              this.listaCatalogo = data;
            },
            error => console.log( error )
          );
  }

}

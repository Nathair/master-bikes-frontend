import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material";
import { RequestDetailUserComponent } from '../request-detail-user/request-detail-user.component';
import { SharedService } from '../../../shared/services/shared.service'

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.css']
})
export class RequestListComponent implements OnInit {

  solicitudes: any;
  solicitud: {} = {
    tipoSolicitud: "",
    fecha: "",
    estado: ""
  }

  constructor( public dialog: MatDialog, private _shared: SharedService ) { }

  ngOnInit() {
    this.obtenerSolicitudes();
  }

  openDialog(value: {}): void {
    let dialogRef = this.dialog.open(RequestDetailUserComponent, {
      data: value
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  obtenerSolicitudes() {
    this._shared.getRequests().subscribe( data => {
      this.solicitudes = data;
    })
  }

}

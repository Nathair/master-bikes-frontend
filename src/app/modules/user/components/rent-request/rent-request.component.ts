import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-rent-request',
  templateUrl: './rent-request.component.html',
  styleUrls: ['./rent-request.component.css']
})
export class RentRequestComponent implements OnInit {

  form;
  check: boolean = true;
  rightDivVisible: boolean = false;

  listaBicis: any[] = [
    {
      id: "1",
      bici: ""
    },
    {
      id: "2",
      bici: ""
    },
    {
      id: "3",
      bici: ""
    },
    {
      id: "4",
      bici: ""
    }
  ]

  constructor( private fb: FormBuilder ) {
    this.form = fb.group({
      tipoBicicleta: ['', Validators.required],
      bicicleta: ['', Validators.required],
      arriendo: ['', Validators.required],
      formaPago: ['', Validators.required],
      fechaEntrega: ['', Validators.required],
      garantia: ['', Validators.required]
    })
  }

  ngOnInit() {
  }

  checkTipoBici() {
    switch(this.form.value.tipoBicicleta) {
      case "1":
        for (let i = 0; i < this.listaBicis.length; i++) {
            this.listaBicis[i].bici = "bicicletaMontaña" + i;
        }
        break;
      case "2":
        for (let i = 0; i < this.listaBicis.length; i++) {
          this.listaBicis[i].bici = "bicicletaRuta" + i;
        }
        break;
      case "3":
        for (let i = 0; i < this.listaBicis.length; i++) {
          this.listaBicis[i].bici = "bicicletaHibrida" + i;
        }
        break;
      case "4":
        for (let i = 0; i < this.listaBicis.length; i++) {
          this.listaBicis[i].bici = "bicicletaUrbana" + i;
        }
        break;
    }
    this.check = false;
  }

  checkSolicitud() {
    if (this.form.valid) {
      this.rightDivVisible = true;
    }
  }

}

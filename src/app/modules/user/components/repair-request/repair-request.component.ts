import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-repair-request',
  templateUrl: './repair-request.component.html',
  styleUrls: ['./repair-request.component.css']
})
export class RepairRequestComponent implements OnInit {

  form;
  visible: boolean = false;

  constructor( private fb: FormBuilder, private router: Router ) {
    this.form = fb.group({
      problema: ['', Validators.required],
      detalle: ['', Validators.required]
    })
  }

  ngOnInit() {
  }

  checkSolicitud() {
    if (this.form.valid) {
      this.visible = true;
    }
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestDetailUserComponent } from './request-detail-user.component';

describe('RequestDetailUserComponent', () => {
  let component: RequestDetailUserComponent;
  let fixture: ComponentFixture<RequestDetailUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestDetailUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestDetailUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

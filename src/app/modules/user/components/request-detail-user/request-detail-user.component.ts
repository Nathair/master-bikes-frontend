import { Component, OnInit, ViewEncapsulation, Inject, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from "@angular/material";
import { Router } from '@angular/router';

@Component({
  selector: 'app-request-detail-user',
  templateUrl: './request-detail-user.component.html',
  styleUrls: ['./request-detail-user.component.css']
})
export class RequestDetailUserComponent implements OnInit {

  idSolicitud: number;
  tipoSolicitud: any;
  fecha: any;
  estado: any;
  visible: boolean = true;

  constructor(public dialogRef: MatDialogRef<RequestDetailUserComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.idSolicitud = this.data.idSolicitud;
    this.tipoSolicitud = this.data.tipoSolicitud;
    this.fecha = this.data.fechaIngreso;
    this.estado = this.data.estadoSolicitud;
  }

  close() {
    this.dialogRef.close();
  }

}

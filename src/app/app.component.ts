import { Component } from '@angular/core';
import { ParameterService } from "./modules/shared/services/parameter.service";
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'app';

  itemValue = '';

  constructor( public db: AngularFirestore,
                private _parameterService: ParameterService ) {
    this.loadParameters();
  }

  // Método para cargar los parametros necesarios para realizar la integracion
  // con los servicios
  loadParameters() {
    this._parameterService.getValues();
  }

  verifyVisible() {
    if (this._parameterService.getFlag() == "1") {
      return true;
    } else {
      return false;
    }
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Componentes
import { LoginComponent } from './modules/login/login.component';
import { UserComponent } from './modules/user/user.component';
import { ExpertComponent } from './modules/expert/expert.component';
import { CatalogueComponent } from './modules/user/components/catalogue/catalogue.component';
import { RepairRequestComponent } from './modules/user/components/repair-request/repair-request.component';
import { RentRequestComponent } from './modules/user/components/rent-request/rent-request.component';
import { RequestListComponent } from './modules/user/components/request-list/request-list.component';
import { RegistryComponent } from './modules/registry/registry.component';
import { RepairDetailComponent } from './modules/user/components/repair-detail/repair-detail.component';
import { RequestDetailUserComponent } from './modules/user/components/request-detail-user/request-detail-user.component';
import { RequestResponseComponent } from './modules/expert/components/request-response/request-response.component';
import { RequestDetailComponent } from './modules/expert/components/request-detail/request-detail.component';
import { StockComponent } from './modules/expert/components/stock/stock.component';

// add las rutas below
const APP_ROUTES: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'registry', component: RegistryComponent },
  {
    path: 'user',
    component: UserComponent,
    children: [
      { path: 'catalogue', component: CatalogueComponent },
      { path: 'repair-request', component: RepairRequestComponent },
      { path: 'rent-request', component: RentRequestComponent },
      { path: 'request-list', component: RequestListComponent },
      { path: 'repair-detail', component: RepairDetailComponent },
      { path: 'request-detail-user', component: RequestDetailUserComponent }
    ]
  },
  {
    path: 'expert',
    component: ExpertComponent,
    children: [
      { path: 'request-response', component: RequestResponseComponent },
      { path: 'request-detail', component: RequestDetailComponent },
      { path: 'stock', component: StockComponent }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forRoot(APP_ROUTES, { useHash: true })],
    exports: [RouterModule]
})

export class APP_ROUTING {}
